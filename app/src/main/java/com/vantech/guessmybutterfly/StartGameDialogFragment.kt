package com.example.guessmybutterfly

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.SeekBar
import android.widget.TextView

class StartGameDialogFragment : ShapedDialogFragment() {
    private var mPath: String? = null

    override fun onCreateView(layoutInflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val contentView = layoutInflater.inflate(R.layout.dialog_fragment_start_game, container, false)
        val seek = contentView.findViewById<SeekBar>(R.id.sb_nrElems)
        val tvNrElems = contentView.findViewById<TextView>(R.id.tv_numberOfElems)
        seek?.setOnSeekBarChangeListener(object :
                SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seek: SeekBar,
                                           progress: Int, fromUser: Boolean) {
                val text = (progress * 2 + 3).toString()
                tvNrElems.setText(text)
            }

            override fun onStartTrackingTouch(seek: SeekBar) {
                // write custom code for progress is started
            }

            override fun onStopTrackingTouch(seek: SeekBar) {
            }
        })
        mPath = arguments!!.getString(PATH)
        contentView!!.findViewById<Button>(R.id.bt_ok_start_game).setOnClickListener(View.OnClickListener {
            val nrOfElems = tvNrElems.text.toString().toInt()
            when (mPath) {
                ACTIVITY_CPU -> {
                    val intent = Intent(context, CpuGameActivity::class.java)
                    intent.putExtra(getString(R.string.total_round_param), nrOfElems)
                    startActivity(intent)
                }
                ACTIVITY_FRIEND -> {
                    /*val intent = Intent(context, FriendGameActivity::class.java)
                    intent.putExtra(getString(R.string.pref_total_round), nrOfElems)
                    startActivity(intent)*/
                }
            }
            dismiss()
        })
        contentView.findViewById<ImageView>(R.id.iv_close).setOnClickListener{
            dialog?.dismiss()
        }
        return contentView
    }

    companion object {
        private const val PATH = "path"
        const val ACTIVITY_CPU = "cpu"
        const val ACTIVITY_FRIEND = "friend"
        const val TAG_CPU_DIALOG = "cpu_dialog"
        const val TAG_FRIEND_DIALOG = "friend_dialog"
        fun getInstance(path: String?): StartGameDialogFragment {
            val winnerDialogFragment = StartGameDialogFragment()
            val args = Bundle()
            args.putString(PATH, path)
            winnerDialogFragment.arguments = args
            return winnerDialogFragment
        }
    }
}