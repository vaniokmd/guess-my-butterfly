package com.example.guessmybutterfly

import androidx.fragment.app.DialogFragment
import com.example.guessmybutterfly.R

open class ShapedDialogFragment : DialogFragment() {
    override fun onStart() {
        super.onStart()
        val dialog = dialog
        if (dialog != null) {
            dialog.window!!.setBackgroundDrawable(resources.getDrawable(R.drawable.bckg_shaped_white))
            dialog.setCanceledOnTouchOutside(false)
        }
    }
}