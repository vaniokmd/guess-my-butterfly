package com.example.guessmybutterfly

import android.content.Context
import android.widget.TableRow

class TableRowCustom : TableRow {
    constructor(context: Context?) : super(context) {
    }
    init {
        layoutParams = LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT)
    }
}