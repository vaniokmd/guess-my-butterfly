package com.example.guessmybutterfly

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.example.guessmybutterfly.StartGameDialogFragment.Companion.ACTIVITY_CPU
import com.example.guessmybutterfly.StartGameDialogFragment.Companion.ACTIVITY_FRIEND
import com.example.guessmybutterfly.StartGameDialogFragment.Companion.TAG_CPU_DIALOG
import com.example.guessmybutterfly.StartGameDialogFragment.Companion.TAG_FRIEND_DIALOG
import hotchemi.android.rate.AppRate

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        AppRate.with(this)
                .setInstallDays(0) // default 10, 0 means install day.
                .setLaunchTimes(3) // default 10
                .setRemindInterval(2) // default 1
                .setShowLaterButton(true) // default true
                .monitor()

        // Show a dialog if meets conditions

        // Show a dialog if meets conditions
        AppRate.showRateDialogIfMeetsConditions(this)
    }

    fun onCpuClick(view: View?) {
        val sharedPref = this.getSharedPreferences(getString(R.string.shared_prefs),Context.MODE_PRIVATE)
        val isGameFinished = sharedPref.getBoolean(getString(R.string.game_finished_param), true)
        if (isGameFinished) {
            StartGameDialogFragment.getInstance(ACTIVITY_CPU)
                .show(supportFragmentManager, TAG_CPU_DIALOG)
        } else {
            val intent = Intent(this, CpuGameActivity::class.java)

            val currentRoundParam = getString(R.string.current_round_param)
            val totalRoundParam = getString(R.string.total_round_param)
            val isNextRoundParam = getString(R.string.is_next_round_param)
            val scoreHostParam = getString(R.string.scor_host_param)
            val scorGuestParam = getString(R.string.scor_guest_param)
            val isNextTurnParam = getString(R.string.is_next_turn_param)
            val hostGuessesParam = getString(R.string.host_guesses_param)
            val guestGuessesParam = getString(R.string.guest_guesses_param)
            val turnParam = getString(R.string.turn_param)
            val setSelectedImgsParam = getString(R.string.set_selected_imgs_param)
            val listOfImgsParam = getString(R.string.list_of_imgs_param)
            val nrImgsParam = getString(R.string.nr_imgs_param)
            val setGuestIndexesParam = getString(R.string.set_guest_indexes_param)

            val currentRound = sharedPref.getInt(currentRoundParam,1)
            intent.putExtra(currentRoundParam,currentRound)
            val totalRound = sharedPref.getInt(totalRoundParam,3)
            intent.putExtra(totalRoundParam,totalRound)
            val isNextRound = sharedPref.getBoolean(isNextRoundParam,false)
            intent.putExtra(isNextRoundParam,isNextRound)
            val hostScor = sharedPref.getString(scoreHostParam,"0")
            intent.putExtra(scoreHostParam, hostScor)
            val guestScor = sharedPref.getString(scorGuestParam,"0")
            intent.putExtra(scorGuestParam, guestScor)
            val isNextTurn = sharedPref.getBoolean(isNextTurnParam,false)
            intent.putExtra(isNextTurnParam,isNextTurn)
            val hostGuesses = sharedPref.getString(hostGuessesParam,"0")
            intent.putExtra(hostGuessesParam, hostGuesses)
            val guestGuesses = sharedPref.getString(guestGuessesParam, "0")
            intent.putExtra(guestGuessesParam, guestGuesses)
            val turn = sharedPref.getString(turnParam, TURN_HOST)
            intent.putExtra(turnParam, turn)
            val setSelectedNrs = sharedPref.getString(setSelectedImgsParam,null)
            intent.putExtra(setSelectedImgsParam,setSelectedNrs )
            val listOfNumbers = sharedPref.getString(listOfImgsParam, null)
            intent.putExtra(listOfImgsParam,listOfNumbers)
            val nrElems = sharedPref.getInt(nrImgsParam,20)
            intent.putExtra(nrImgsParam,nrElems)
            val setIndexes = sharedPref.getString(setGuestIndexesParam,null)
            intent.putExtra(setGuestIndexesParam,setIndexes)
            startActivity(intent)
        }
    }

    fun onFriendClick(view: View?) {
        StartGameDialogFragment.getInstance(ACTIVITY_FRIEND)
            .show(supportFragmentManager, TAG_FRIEND_DIALOG)
    }

    companion object {
        val TURN_HOST = "host"
        val TURN_GUEST = "guest"
    }
}