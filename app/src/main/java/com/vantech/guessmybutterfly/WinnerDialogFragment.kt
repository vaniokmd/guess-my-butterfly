package com.example.guessmybutterfly

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView

class WinnerDialogFragment : ShapedDialogFragment() {
    override fun onCreateView(layoutInflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val contetView = layoutInflater.inflate(R.layout.dialog_fragment_winner, container, false)
        val winnerIcon = contetView.findViewById<ImageView>(R.id.iv_winner)
        val winnerName = contetView.findViewById<TextView>(R.id.tv_winner_name)
        val winner = arguments!!.getString(WINNER)
        when (winner) {
            WINNER_HOST -> {
                winnerIcon.setImageResource(R.mipmap.ic_host_player)
                winnerName.setText(R.string.host)
            }
            WINNER_GUEST -> {
                winnerIcon.setImageResource(R.mipmap.ic_cpu_player)
                winnerName.setText(R.string.cpu)
            }
            NO_WINNER -> {
                winnerIcon.visibility = View.GONE
                winnerName.visibility = View.GONE
                contetView.findViewById<TextView>(R.id.tv_winner_text).text = getText(R.string.no_winners)
            }
        }
        contetView.findViewById<Button>(R.id.bt_ok).setOnClickListener {
            dialog?.dismiss()
        }
        return contetView
    }
    companion object {
        private val WINNER = "winner"
        val TAG_WINNER_DIALOG_FRAGMENT = "tag_winner_dialog_fragment"
        val WINNER_HOST = "winner_cpu"
        val WINNER_GUEST = "winner_guest"
        val NO_WINNER = "no_winner"
        fun getInstance(winner: String): WinnerDialogFragment {
            val winnerDialogFragment = WinnerDialogFragment()
            val args: Bundle = Bundle()
            args.putString(WINNER, winner)
            winnerDialogFragment.arguments = args
            return winnerDialogFragment
        }
    }
}