package com.example.guessmybutterfly

import android.content.Context
import android.content.Intent
import android.graphics.PorterDuff
import android.os.*
import android.view.View
import android.view.animation.AlphaAnimation
import android.view.animation.Animation
import android.widget.ImageView
import android.widget.TableLayout
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.example.guessmybutterfly.MainActivity.Companion.TURN_GUEST
import com.example.guessmybutterfly.MainActivity.Companion.TURN_HOST
import java.lang.reflect.Type
import java.util.*
import java.util.function.Consumer
import kotlin.properties.Delegates


class CpuGameActivity : AppCompatActivity() {
    private lateinit var mTurn: String
    private var mGuessesTextColor: Int? = null

    private var mListSelectedNumbers = LinkedList<Int>()
    private var mListOfNumbers = LinkedList<Int>()
    private var mSetRandomIndexes = mutableSetOf<Int>()

    private val mMaxSelection = 10
    private var mNrElems = 20
    private var mTotalRounds by Delegates.notNull<Int>()
    private var mCurrentRound: Int = 1

    private var mIsCpuReady: Boolean = false
    private var mIsNextTurn: Boolean = false
    private var mIsNextRound: Boolean = false
    private var mIsNewGame: Boolean = false

    private var mRoundText: String? = null
    private lateinit var mRoundValue: TextView
    private lateinit var mWrapNumbers: TableLayout
    private lateinit var mFabSync: FloatingActionButton
    private lateinit var mHostScor: TextView
    private lateinit var mGuestScor: TextView
    private lateinit var mTurnHost: ImageView
    private lateinit var mTurnGuest: ImageView
    private lateinit var mCountGuest: TextView
    private lateinit var mCountHost: TextView
    private lateinit var mCounterGuessesHost: TextView
    private lateinit var mCounterGuessesGuest: TextView

    @RequiresApi(Build.VERSION_CODES.N)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_cpu_game)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        mWrapNumbers = findViewById(R.id.wrap_numbers)
        setGuestAvatar()
        mCountHost = findViewById(R.id.tv_count_host)
        mCountGuest = findViewById(R.id.tv_count_guest)
        mWrapNumbers.post {
            val gson = Gson()
            val listType: Type = object : TypeToken<LinkedList<Int?>?>() {}.type
            val setType: Type = object : TypeToken<MutableSet<Int?>?>() {}.type
            val listOfNumbersStr = intent.getStringExtra(getString(R.string.list_of_imgs_param))
            val listOfNumbers = gson.fromJson<LinkedList<Int>>(listOfNumbersStr, listType)
            if (listOfNumbers != null) {
                mListOfNumbers = listOfNumbers
            }
            mNrElems = intent.getIntExtra(getString(R.string.nr_imgs_param), 20)
            initWrapNumbers()
            val selectedNumbersStr = intent.getStringExtra(getString(R.string.set_selected_imgs_param))
            val listSelectedNumbers = gson.fromJson<LinkedList<Int>>(selectedNumbersStr, listType)
            val setGuestIndexesStr = intent.getStringExtra(getString(R.string.set_guest_indexes_param))
            val setGuestIndexes = gson.fromJson<MutableSet<Int>>(setGuestIndexesStr, setType)
            if (listSelectedNumbers != null && setGuestIndexes != null) {
                mListSelectedNumbers = listSelectedNumbers
                runColorateHostNumbers()
                mSetRandomIndexes = setGuestIndexes
                if (mIsNextRound || mIsNextTurn) {
                    runCalcolateNrGuesses()
                }
                mIsCpuReady = true

            } else {
                Handler().postDelayed({ startCountCPU() }, 2000)
                startCpuSelection()
            }
            mCountHost.text = mListSelectedNumbers.size.toString()
            mCountGuest.text = mSetRandomIndexes.size.toString()
        }
        mRoundValue = findViewById(R.id.tv_round_text)
        mHostScor = findViewById(R.id.tv_scor_host)
        mGuestScor = findViewById(R.id.tv_scor_guest)
        mTurnHost = findViewById(R.id.iv_turn_host)
        mTurnGuest = findViewById(R.id.iv_turn_guest)
        mCounterGuessesHost = findViewById(R.id.tv_count_guesses_host)
        mCounterGuessesGuest = findViewById(R.id.tv_count_guesses_guest)
        mRoundText = resources.getString(R.string.round_param)
        mFabSync = findViewById(R.id.fab_checkNumbers)
        mFabSync.setOnClickListener {
            val nrToSelect = (mMaxSelection - mListSelectedNumbers.size)
            if (nrToSelect != 0) {
                val text = getString(R.string.select_another) + " " + nrToSelect + " " + getString(R.string.numbers)
                Toast.makeText(applicationContext, text, Toast.LENGTH_SHORT).show()
            } else if (!mIsNextTurn && !mIsNextRound && !mIsNewGame && mIsCpuReady) {
                updateGame()
            } else if (mIsNextTurn) {
                nextTurn()
            } else if (mIsNextRound) {
                nextRound()
            } else if (mIsNewGame) {
                newGame()
            }
        }
        mHostScor.text = intent.getStringExtra(getString(R.string.scor_host_param)) ?: 0.toString()
        mGuestScor.text = intent.getStringExtra(getString(R.string.scor_guest_param)) ?: 0.toString()
        mIsNextTurn = intent.getBooleanExtra(getString(R.string.is_next_turn_param), false)
        mIsNextRound = intent.getBooleanExtra(getString(R.string.is_next_round_param), false)
        mTurn = intent.getStringExtra(getString(R.string.turn_param)) ?: TURN_HOST
        mCounterGuessesHost.text = intent.getStringExtra(getString(R.string.host_guesses_param))
                ?: 0.toString()
        mCounterGuessesGuest.text = intent.getStringExtra(getString(R.string.guest_guesses_param))
                ?: 0.toString()
        mCurrentRound = intent.getIntExtra(getString(R.string.current_round_param),1)
        mTotalRounds = intent.getIntExtra(getString(R.string.total_round_param),3)
        setRoundText()
        setButtonImg()
        setTurnVisibility()
        if (!(mIsNextRound || mIsNextTurn || mIsNewGame)) {
            setTurnAnimation()
        }
    }

    private fun updateGame() {
        val numberGuesses = runCalcolateNrGuesses()
        var playerName: String? = null
        when (mTurn) {
            TURN_HOST -> {
                mIsNextTurn = true
                setButtonImg()
                playerName = resources.getString(R.string.host)
                mCounterGuessesHost.text = numberGuesses.toString()
                mTurnHost.clearAnimation()
            }
            TURN_GUEST -> {
                playerName = resources.getString(R.string.cpu)
                mCounterGuessesGuest.text = numberGuesses.toString()
                val hostGuesses = mCounterGuessesHost.text.toString().toInt()
                val guestGuesses = mCounterGuessesGuest.text.toString().toInt()
                if (hostGuesses > guestGuesses) {
                    mHostScor.text = (mHostScor.text.toString().toInt().plus(1)).toString()
                } else if (hostGuesses < guestGuesses) {
                    mGuestScor.text = (mGuestScor.text.toString().toInt().plus(1)).toString()
                }
                if (mCurrentRound < mTotalRounds!!) {
                    mIsNextRound = true
                    setButtonImg()
                    mTurnGuest.clearAnimation()
                } else if (mCurrentRound == mTotalRounds!!) {
                    mIsNewGame = true
                    setButtonImg()
                    val scorHost = mHostScor.text.toString().toInt()
                    val scorGuest = mGuestScor.text.toString().toInt()
                    var winner: String
                    if (scorHost > scorGuest) {
                        winner = WinnerDialogFragment.WINNER_HOST
                    } else if (scorHost == scorGuest) {
                        winner = WinnerDialogFragment.NO_WINNER
                    } else {
                        winner = WinnerDialogFragment.WINNER_GUEST
                    }
                    WinnerDialogFragment.getInstance(winner).show(supportFragmentManager, WinnerDialogFragment.Companion.TAG_WINNER_DIALOG_FRAGMENT)
                    mTurnGuest.clearAnimation()
                }
            }
        }
        var dialogText = playerName + " " + getString(R.string.guess) + " " + numberGuesses + " " + getString(R.string.numbers)
        Toast.makeText(applicationContext, dialogText, Toast.LENGTH_LONG).show()
    }

    private fun runColorateHostNumbers() {
        for (elem in mListSelectedNumbers) {
            mWrapNumbers.findViewById<ImageViewStyled>(elem)?.background?.setColorFilter(resources.getColor(R.color.colorHost), PorterDuff.Mode.DARKEN)
        }
    }

    private fun runCalcolateNrGuesses(): Int {
        var numberGuesses = 0
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            mSetRandomIndexes.forEach(Consumer { index ->
                var choosenNumber = mListOfNumbers[index]
                if (mListSelectedNumbers.contains(choosenNumber)) {
                    numberGuesses++
                    when (mTurn) {
                        TURN_HOST -> mWrapNumbers.findViewById<ImageViewStyled>(choosenNumber).background.setColorFilter(getResources().getColor(R.color.colorGuessesHost), PorterDuff.Mode.DARKEN)
                        TURN_GUEST -> mWrapNumbers.findViewById<ImageViewStyled>(choosenNumber).background.setColorFilter(getResources().getColor(R.color.colorGuessesGuest), PorterDuff.Mode.DARKEN)
                    }
                } else {
                    mWrapNumbers.findViewById<ImageViewStyled>(choosenNumber).background.setColorFilter(getColor(R.color.colorGuest), PorterDuff.Mode.DARKEN)
                }
            })
        }
        return numberGuesses
    }

    private fun setButtonImg() {
        if (mIsNextRound) {
            mFabSync.setImageResource(R.drawable.ic_next_round)
        } else if (mIsNextTurn) {
            mFabSync.setImageResource(R.drawable.ic_next_turn)
        } else if (mIsNewGame) {
            mFabSync.setImageResource(R.drawable.ic_add)
        } else {
            mFabSync.setImageResource(R.drawable.ic_sync)
        }
    }

    private fun getTurnAnimation(): Animation? {
        val anim: Animation = AlphaAnimation(0.0f, 1.0f)
        anim.duration = 500 //You can manage the blinking time with this parameter
        anim.startOffset = 200
        anim.repeatMode = Animation.REVERSE
        anim.repeatCount = Animation.INFINITE
        return anim
    }

    private fun setTurnVisibility() {
        when (mTurn) {
            TURN_HOST -> {
                mTurnHost.visibility = View.VISIBLE
                mTurnHost.clearAnimation()
                mTurnGuest.visibility = View.GONE
                mTurnGuest.clearAnimation()
            }
            TURN_GUEST -> {
                mTurnGuest.visibility = View.VISIBLE
                mTurnGuest.clearAnimation()
                mTurnHost.visibility = View.GONE
                mTurnHost.clearAnimation()
            }
        }
    }

    private fun setTurnAnimation() {
        when (mTurn) {
            TURN_HOST -> {
                mTurnHost.startAnimation(getTurnAnimation())
                mTurnGuest.clearAnimation()
            }
            TURN_GUEST -> {
                mTurnGuest.startAnimation(getTurnAnimation())
                mTurnHost.clearAnimation()
            }
        }
    }

    private fun changeTurn() {
        when (mTurn) {
            TURN_HOST -> {
                mTurn = TURN_GUEST
            }
            TURN_GUEST -> {
                mTurn = TURN_HOST
            }
        }
    }

    private fun setRoundText() {
        mRoundValue.text = mRoundText + " " + mCurrentRound + "/" + mTotalRounds
    }

    private fun incrRound() {
        ++mCurrentRound
    }

    private fun setGuestAvatar() {
        findViewById<ImageView>(R.id.iv_avatar_guest).setImageResource(R.mipmap.ic_cpu_player)
    }

    private fun initWrapNumbers() {
        val nrColums = 5
        val nrRows: Int = mNrElems.div(5)
        val matrixSize = nrColums * nrRows
        var nrElem = 0;
        for (nrRow in 0 until nrRows) {
            var tableRow = TableRowCustom(this)
            mWrapNumbers.addView(tableRow)
            for (colomn in 0 until nrColums) {
                var textView = ImageViewStyled(this)
                var randomNumber: Int
                if (mListOfNumbers.size == matrixSize) {
                    randomNumber = mListOfNumbers[nrElem++]
                } else {
                    randomNumber = (Math.random() * 100).toInt()
                    while (mListOfNumbers.contains(randomNumber)) {
                        randomNumber = (Math.random() * 100).toInt()
                    }
                    mListOfNumbers.add(randomNumber)
                }
                textView.id = randomNumber
                textView.setImageResource(randomNumber)
                setTextViewOnClickListener(textView)
                tableRow.addView(textView)
            }
        }
    }

    private fun setTextViewOnClickListener(textView: ImageViewStyled) {
        val vibrator = applicationContext?.getSystemService(Context.VIBRATOR_SERVICE) as Vibrator
        textView.setOnClickListener {
            if (!mIsNextRound && !mIsNextTurn && !mIsNewGame) {
                if (mListSelectedNumbers.contains(textView.id)) {
                    textView.background.clearColorFilter()
                    mListSelectedNumbers.remove(textView.id)
                } else if (mListSelectedNumbers.size < mMaxSelection) {
                    textView.background.setColorFilter(resources.getColor(R.color.colorHost), PorterDuff.Mode.DARKEN)
                    mListSelectedNumbers.add(textView.id)
                    if (Build.VERSION.SDK_INT >= 26) {
                        vibrator.vibrate(VibrationEffect.createOneShot(200, VibrationEffect.DEFAULT_AMPLITUDE))
                    } else {
                        vibrator.vibrate(200)
                    }
                }
                mCountHost.text = mListSelectedNumbers.size.toString()
            }
        }
    }

    private fun startCpuSelection() {
        for (i in 0 until mMaxSelection) {
            var randomIndex = (Math.random() * mListOfNumbers.size).toInt()
            while (mSetRandomIndexes.contains(randomIndex)) {
                randomIndex = (Math.random() * mListOfNumbers.size).toInt()
            }
            mSetRandomIndexes.add(randomIndex)
        }
    }

    private fun startCountCPU() {
        var times = 0
        val handler = Handler()
        handler.postDelayed(object : Runnable {
            override fun run() {
                ++times
                if (times == 11) {
                    mIsCpuReady = true
                    Toast.makeText(applicationContext, R.string.cpu_ready, Toast.LENGTH_SHORT).show()
                    handler.removeCallbacks(this)
                } else {
                    mCountGuest.text = times.toString()
                    handler.postDelayed(this, (Math.random() * 1000).toLong())
                }
            }
        }, (Math.random() * 1000).toLong())
    }

    private fun nextTurn() {
        mIsCpuReady = false
        mIsNextTurn = false
        setButtonImg()
        changeTurn()
        setTurnVisibility()
        setTurnAnimation()
        clearColorFilter()
        clearListsForNextTurn()
        startCpuSelection()
        startCountCPU()
    }

    private fun nextRound() {
        mIsNextRound = false
        mIsCpuReady = false
        setButtonImg()
        changeTurn()
        setTurnVisibility()
        setTurnAnimation()
        incrRound()
        setRoundText()
        mCounterGuessesHost.text = "0"
        mCounterGuessesGuest.text = "0"
        mCountHost.text = "0"
        mCountGuest.text = "0"
        mNrElems = mNrElems?.plus(5)
        clearListsForNextLevel()
        clearWrapNumbers()
        initWrapNumbers()
        startCpuSelection()
        startCountCPU()
    }

    private fun newGame() {
        StartGameDialogFragment.getInstance(StartGameDialogFragment.ACTIVITY_CPU).show(supportFragmentManager, StartGameDialogFragment.TAG_CPU_DIALOG)
    }

    private fun clearColorFilter() {
        for (id in mSetRandomIndexes) {
            mWrapNumbers.findViewById<TextView>(mListOfNumbers.get(id))?.background?.clearColorFilter()
        }
        for (id in mListSelectedNumbers) {
            mWrapNumbers.findViewById<TextView>(id)?.background?.clearColorFilter()
        }
    }

    private fun clearListsForNextTurn() {
        mSetRandomIndexes.clear()
        mListSelectedNumbers.clear()
    }

    private fun clearListsForNextLevel() {
        clearListsForNextTurn()
        mListOfNumbers.clear()
    }

    private fun clearWrapNumbers() {
        mWrapNumbers.removeAllViews()
    }

    private fun goToMainPage() {
        startActivity(Intent(this, MainActivity::class.java))
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    override fun onBackPressed() {
        super.onBackPressed()
        goToMainPage()
    }

    override fun onStop() {
        super.onStop()
        val sharedPref = this.getSharedPreferences(getString(R.string.shared_prefs), Context.MODE_PRIVATE)
                ?: return
        with(sharedPref.edit()) {
            if (!mIsNewGame) {
                val gson = Gson()
                putString(getString(R.string.scor_host_param), mHostScor.text.toString())
                putString(getString(R.string.scor_guest_param), mGuestScor.text.toString())

                putInt(getString(R.string.current_round_param), mCurrentRound)
                putInt(getString(R.string.total_round_param), mTotalRounds)
                putString(getString(R.string.turn_param), mTurn)
                putBoolean(getString(R.string.is_next_turn_param), mIsNextTurn)
                putBoolean(getString(R.string.is_next_round_param), mIsNextRound)
                putString(getString(R.string.host_guesses_param), mCounterGuessesHost.text.toString())
                putString(getString(R.string.guest_guesses_param), mCounterGuessesGuest.text.toString())

                putString(getString(R.string.set_selected_imgs_param), gson.toJson(mListSelectedNumbers));
                putString(getString(R.string.list_of_imgs_param), gson.toJson(mListOfNumbers))
                putString(getString(R.string.set_guest_indexes_param), gson.toJson(mSetRandomIndexes))
                putInt(getString(R.string.nr_imgs_param), mNrElems)
            }
            putBoolean(getString(R.string.game_finished_param), mIsNewGame)
            apply()
    }
    }
}