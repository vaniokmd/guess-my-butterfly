package com.example.guessmybutterfly

import android.content.Context
import android.content.res.Resources
import android.graphics.Typeface
import android.util.AttributeSet
import android.view.Gravity
import android.widget.ImageView
import android.widget.TableRow

class ImageViewStyled:
    androidx.appcompat.widget.AppCompatImageView {
    constructor(context: Context?) : super(context) {
        init()
    }
    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs) {init()}
    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {init()}
    fun init() {
        setBackgroundResource(R.drawable.bck_shaped_bordered)
        layoutParams = getCustomLayoutParams()
        val padding = convertDpAsPixels(8)
        setPadding(padding,padding,padding,padding)
    }

    private fun getCustomLayoutParams(): TableRow.LayoutParams {
        val customLayoutParams = TableRow.LayoutParams(0, TableRow.LayoutParams.WRAP_CONTENT,1f)
        val margin = convertDpAsPixels(2)
        customLayoutParams.setMargins(margin,margin,margin,margin)
        return customLayoutParams
    }
    private fun convertDpAsPixels(sizeInDp: Int): Int {
        val scale = Resources.getSystem().displayMetrics.density
        return (sizeInDp * scale + 0.5f).toInt()
    }
}